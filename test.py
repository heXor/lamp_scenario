#!/usr/bin/env python3
# coding: utf-8

import sys
import re
import io
import os
import csv
import time
import sched
import logging
from pathlib import Path
import glob
import paho.mqtt.client as mqtt  # pip install paho-mqtt
# later - import pyexcel as pe # pip install pyexcel & pip install pyexcel-ods
from docopt import docopt
from math import *
from pprint import *
from io import StringIO
import asyncio
import aiomqtt  # pip install aiomqtt
import datetime
import random



async def demo():
    """ пример использования aiomqtt """
    loop = asyncio.get_event_loop()

    c = aiomqtt.Client(loop)
    c.loop_start()
    c.username_pw_set('ycsmaklj', 'R3KeL8nbTR4G')

    connected = asyncio.Event(loop=loop)

    def on_connect(client, userdata, flags, rc):
        connected.set()

    c.on_connect = on_connect

    await c.connect('m16.cloudmqtt.com', 12810)
    #await c.connect("192.168.1.19")
    await connected.wait()

    print("Connected!")

    subscribed = asyncio.Event(loop=loop)

    def on_subscribe(client, userdata, mid, granted_qos):
        subscribed.set()

    c.on_subscribe = on_subscribe

    c.subscribe("test/#")

    def write_event(client, userdata, msg):
        print("msg:", msg.topic, '=', msg.payload.decode())

    c.message_callback_add("test/#", write_event)

    await subscribed.wait()

    #def on_message(client, userdata, message):
    #    print("Got message (all msg):", message.topic, message.payload)
    #c.on_message = on_message

    await c.publish("test/read6", "2", retain=True).wait_for_publish()
    await c.publish("test/read6_2", "3", retain=True).wait_for_publish()

    await asyncio.sleep(60*60, loop=loop)

    print("Disconnecting...")

    disconnected = asyncio.Event(loop=loop)

    def on_disconnect(client, userdata, rc):
        disconnected.set()

    c.on_disconnect = on_disconnect
    c.disconnect()
    await disconnected.wait()

    print("Disconnected")

    await c.loop_stop()
    print("MQTT loop stopped!")


def main():
    asyncio.get_event_loop().run_until_complete(demo())

if __name__ == "__main__":
    main()
